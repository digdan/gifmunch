import ioredis from 'ioredis';
const state = {
  db: null,
}

export function initializeDb( callback ) {
  state.db = new ioredis(process.env.REDIS_URI);
  state.db.on("connect", function() {
    callback( state.db );
  });
}
