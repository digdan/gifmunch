import fs from 'fs';
import path from 'path';
import rimraf from 'rimraf';
import s3fs from 's3fs';
import {_extend} from 'util';

let storedConfig;

export function janitorResync(db) {
	//Connect to s3
	let s3Config = _extend(storedConfig.s3.options, {
		accessKeyId : process.env.AWS_S3_KEY,
		secretAccessKey : process.env.AWS_S3_SECRET
	});
	const lister = new s3fs(storedConfig.s3.bucket, s3Config);
	lister.readdirp('/').then(files => {
		files.forEach( file => {
			let pathParts = path.parse(file);
			db.exists(pathParts.name).then(result => {
				if (result == 0) {
					//Remove from S3
					s3fs.unlink(file);
					/* Remove from lists
					db.zrem('gifList', pathParts.name);
					db.zrem('gifLastUsed', pathParts.name);
					*/
				}
			});
		});
		return;
	}, reason => {
		console.log('janitorSync error ', reason);
		return;
	});
	//Iterate through each S3 object
	//Check against redis
	//Delete if not found, or expired
}

function clean() {
	fs.readdir(storedConfig.uploadsDir, function(err, files) {
	  files.forEach(function(file, index) {
	    fs.stat(path.join(storedConfig.uploadsDir, file), function(err, stat) {
	      var endTime, now;
	      if (err) {
	        return console.error(err);
	      }
	      now = new Date().getTime();
	      endTime = new Date(stat.ctime).getTime() + storedConfig.tableAge;
	      if (now > endTime) {
	        return rimraf(path.join(storedConfig.uploadsDir, file), function(err) {
	          if (err) {
	            return console.error(err);
	          }
	          console.log('successfully deleted ',file);
	        });
	      }
	    });
	  });
	});
}

export default config => {
	storedConfig = config;
	setInterval(clean, config.tableAge);
}
