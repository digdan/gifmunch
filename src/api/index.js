import { Router } from 'express';
import munch from './munch';
import {janitor, janitorResync} from '../janitor';

export default ({ config, db }) => {
	let api = Router();

	// mount the facets resource
	api.use('/munch', munch({ config, db }));

	// perhaps expose some API metadata at the root
	api.get('/', (req, res) => {
		res.send('ok');
	});

	api.get('/resync', (req, res) => {
		janitorResync(db);
		res.send('done');
	});

	return api;
}
