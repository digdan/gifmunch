import {Router} from 'express';
import {exec} from 'child_process';
import url from 'url';
import path from 'path';
import request from 'request';
import cheerio from 'cheerio';
import fs from 'fs';
import {_extend} from 'util';
import s3fs from 's3fs';
import crypto from 'crypto';

//Config during execution
let storedConfig;

const desiredKeys = [
	'ident',
	'size',
	'mediaURL',
	'url',
	's3File',
	'codec',
	'bitrate',
	'frames',
	'height',
	'width',
	'duration'
];

function makeID (size) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < size; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}

function redirectToSource(requestURL, callback) {
	var options = {
		url: requestURL,
		timeout: 1500,
	};
	request.get(options, function(err, response, body) {
		//Fail on error
		if (err) return callback(false);

		const $ = cheerio.load(body);
		const result = $('meta[itemprop="contentURL"]').attr('content');
		if (result === undefined) return callback(false);
		ifExists(result, function(cb) {
			return callback(cb);
		});
	});
}

function ifExists(requestURL, callback) {
	const myURL = url.parse(requestURL);
	if (myURL.path == null) return callback(false);
	var options = {
		url: requestURL,
		timeout: 1500,
	};

	request.head(options, function(err, response, body) {
		const details = {
			url:requestURL,
			mediaURL:requestURL,
			mimeType:'',
			size:0,
		};

		//Fail on error
		if (err) return callback(false);

		//Recognized MimeType
		if (new RegExp(storedConfig.acceptedMimes.join("|")).test(response.headers["content-type"])) {
			details.mimeType = response.headers["content-type"];
			details.size = response.headers["content-length"];
			return callback(details);
		} else if (new RegExp(storedConfig.redirectMimes.join("|")).test(response.headers["content-type"])) {
			return redirectToSource(requestURL, function(secondDetails) {
				details.mediaURL = secondDetails.url;
				details.size = secondDetails.size;
				details.mimeType = secondDetails.mimeType;
				return callback(details);
			});
		} else { //Unknown MimeType
			console.log("Unknown Mime-type",response.headers["content-type"]);
			return callback(false);
		}
	});
}

function downloadFile(url, path, callback) {
	const hash = crypto.createHash('md5');
	request({uri: url})
		.pipe(fs.createWriteStream(path))
		.on('close', function() {
			const ident = hash.update(fs.readFileSync(path, 'utf8'),'utf8').digest('hex');
			const result = {
				origFile:path,
				ident
			};
			callback(result);
		});
}

function ffconvert(file, newFile, callback) {
	exec('/usr/bin/docker run --rm -v /dockers/vols/gifmunch-table:'+storedConfig.uploadsDir+' --name ffmpeg iwd/ffmpeg ffmpeg -i '+file+' -c:v libx264 '+newFile, (err, stdout, stderr) => {
		callback(newFile);
	});
}

function ffprobe(file,callback) {
	exec('/usr/bin/docker run --rm -v /dockers/vols/gifmunch-table:/table --name ffmpeg iwd/ffmpeg ffprobe -v quiet -print_format json -show_format -show_streams '+file, (err, stdout, stderr) => {
		if (err) {
			return callback(err);
		}
		const commandReturn = JSON.parse(stdout);
		let results = {
			duration : commandReturn.streams[0].duration,
			width : commandReturn.streams[0].width,
			height : commandReturn.streams[0].height,
			frames : commandReturn.streams[0].nb_frames,
			ratio : commandReturn.streams[0].display_aspect_ratio,
			bitrate : commandReturn.streams[0].bit_rate,
			codec : commandReturn.streams[0].codec_name,
		};
		callback( results );
	});
}

function s3UploadFile(config, result, callback) {
	let s3Config = _extend(config.s3.options, {
		accessKeyId : process.env.AWS_S3_KEY,
		secretAccessKey : process.env.AWS_S3_SECRET
	});
	const uploader = new s3fs(config.s3.bucket, s3Config);
	const ws = uploader.createWriteStream(result.s3File);
	ws.on('error', function(err) {
		console.log('upload error',err);
	});
	ws.on('finish', function() {
		callback();
	});
	fs.createReadStream(result.file).pipe(ws);
}

function redisSave(db, result, callback) {
	// Filter only wanted fields
	let savableResult = {};
	desiredKeys.forEach( item => {
		savableResult[item] = result[item];
	});
	savableResult.lastUsed = Date.now();
	db.hmset(savableResult.ident, savableResult);
	db.zadd('gifList', Date.now(), savableResult.ident);
	db.zadd('gifLastUsed', Date.now(), savableResult.ident);
	// Save multi hash
	callback(null, savableResult);
}

function redisCheck(db, result, callback) {
	let endResult;
	endResult = db.hmget(result.ident);
	console.log(endResult);
	callback(null, endResult);
}

function redisCheck(db, ident, callback) {
	return callback(false);
}

export default ({ config, db }) => {
	let router = Router();
	storedConfig = config;
	router.get('/*', function(req,res) {
		console.log('munch request : ', req.params[0]);
		ifExists(req.params[0], function(details) {
			if (details == false) {
				res.send('nope');
			} else {
				const fileID = config.uploadsDir+'/'+makeID(12)+path.parse(details.mediaURL).ext;
				if (details.mediaURL == undefined) details.mediaURL = details.url;
				downloadFile(details.mediaURL, fileID, function(downloadResult) {
					let result = _extend(downloadResult,details);
					redisCheck(db, result, (err, endResult) => {

					});
					//TODO -- Check for ident destination file before converting. If exists, return its record.
					ffconvert(result.origFile, storedConfig.uploadsDir+'/'+result.ident+'.mp4', function(newFile) {
						result.file = newFile;
						result.s3File = result.ident.substr(0,2)+'/'+result.ident+'.mp4';
						ffprobe(result.file, function(info) {
							result = _extend(result,info);
							// Store video in S3
							s3UploadFile(config,result, function(ret) {
								// Save to REDIS by ID and by url
								redisSave(db, result, function(err, endResult) {
									if (err) {
										console.log('redis save error', err);
										res.send('nope');
										return;
									}
									console.log('result', endResult);
									res.send(JSON.stringify(endResult));
									return;
								});
							});
						});
					});
				});
			}
		});
	});
	return router;
};
